package mpr_streams.mpr_streams;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class OrdersService {

    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
    	List<Order> ordersWhichHaveMoreThan5OrderItems = orders.stream()
    			.filter(order -> order.getItems().size() > 5)
    			.collect(Collectors.toList());
    	return ordersWhichHaveMoreThan5OrderItems;
    }
    
    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
    	ClientDetails oldestClient = orders.stream()
    			.map(order -> order.getClientDetails())
    			.max(Comparator.comparing(ClientDetails::getAge))
    			.get();
    	return oldestClient;		
    }

    public static Order findOrderWithLongestComments(List<Order> orders) {
/*    	int maximum = orders.stream()
    			.map(order -> order.getComments().length())
    			.max(null)
    			.get();
    			
     			.max(Comparator.comparing(Order::getComments))
    			.get();
    	Optional<Order> orderWithLongestComments = orders.stream()
    			.filter(order -> order.getComments().length() == maximum)
    			.findAny();*/
    	
    	Order orderWithLongestComments = orders.stream()
    			.max(Comparator.comparing(order -> order.getComments().length()))  //!!!
    			.get();
    	
    	return orderWithLongestComments;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
    	String joined = orders.stream()
    			.filter(order -> order.getClientDetails().getAge() > 18)
    			.map(order -> order.getClientDetails().getName() + " " + order.getClientDetails().getSurname())
    			.collect(Collectors.joining("; "));
    	return joined;
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
/*    	List<List<OrderItem>> orderItemsWithComments = orders.stream()
    			.filter(order -> order.getComments().startsWith("A"))
    			.map(order -> order.getItems())
    			.collect(Collectors.toList());
    	
    	List<String> orderItemsNames = new ArrayList<>();
    	
    	for (List<OrderItem> ll : orderItemsWithComments){
    		for (OrderItem l : ll){
    			orderItemsNames.add(l.getName());
        	}
    	}
    	
    	List<String> sortedOrderItemsNamesWithComments = orderItemsNames.stream()
    			.sorted()
    			.collect(Collectors.toList());*/

    	List<String> sortedOrderItemsNamesWithComments = orders.stream()
    			//.filter(order -> order.getComments().startsWith("A"))
    			//.filter(order -> Pattern.matches("[aA][a-zA-Z0-9 ]+", order.getComments()) )  //ok
    			.filter(order -> order.getComments().matches("[aA][a-zA-Z0-9 ]+"))
    			.map(order -> order.getItems())
    			.flatMap(lista -> lista.stream())
    			.map(item -> item.getName())
    			.distinct()
    			.sorted()
    			.collect(Collectors.toList());
    	return sortedOrderItemsNamesWithComments;
    	
    }  

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
//    	List<String> capitalizedClientsLogins = orders.stream()
//    			.filter(order -> order.getClientDetails().getName().startsWith("S"))
//    			.map(order -> order.getClientDetails().getLogin().toUpperCase())
//    			.collect(Collectors.toList());
//    	System.out.println(capitalizedClientsLogins);
    	orders.stream()
    			.filter(order -> order.getClientDetails().getName().startsWith("S"))
    			.map(order -> order.getClientDetails().getLogin().toUpperCase())
    			.forEach(System.out::print);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
    	Map<ClientDetails, List<Order>> ordersByClient = orders.stream()
    			.collect(Collectors.groupingBy(Order::getClientDetails));
    	return ordersByClient;
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
    	Map<Boolean, List<ClientDetails>> clientsByUnderAndOver18 = orders.stream()
    			.map(order -> order.getClientDetails())
    			.collect(Collectors.partitioningBy(client -> client.getAge() > 18));
    	return clientsByUnderAndOver18;
    	
    }

}