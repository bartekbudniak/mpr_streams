package mpr_streams.mpr_streams;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OrdersServiceTest {
	
	ClientDetails aClient;
	ClientDetails bClient;
	ClientDetails cClient;

	OrderItem item1;
	OrderItem item2;
	OrderItem item3;
	OrderItem item4;
	OrderItem item5;
	OrderItem item6;
	OrderItem item7;
	OrderItem item8;
	
	List<OrderItem> aItems;
	List<OrderItem> bItems;
	List<OrderItem> cItems;
	
	Order aOrder;
	Order bOrder;
	Order cOrder;
	
	List<Order> orders;
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	@Before
	public void setup(){
		
		aClient = new ClientDetails("aLogin","aName","aSurname",15);
		bClient = new ClientDetails("bLogin","bName","bSurname",19);
		cClient = new ClientDetails("cLogin","SName","cSurname",21);

		item1 = new OrderItem(1,"a");
		item2 = new OrderItem(2,"b");
		item3 = new OrderItem(3,"c");
		item4 = new OrderItem(4,"d");
		item5 = new OrderItem(5,"de");
		item6 = new OrderItem(6,"e");
		item7 = new OrderItem(7,"f");
		item8 = new OrderItem(8,"g");

		aItems= Arrays.asList(item1, item2, item3, item4, item5);
		bItems= Arrays.asList(item1, item2, item3, item4, item5, item6, item7, item8);
		cItems= Arrays.asList(item1, item2, item3, item4);
		
		aOrder = new Order(aClient, aItems, "A to jest komentarz");
		bOrder = new Order(bClient, bItems, "Short");
		cOrder = new Order(cClient, cItems, "a comment");
		
		orders = Arrays.asList(aOrder, bOrder, cOrder);
		
		System.setOut(new PrintStream(outContent));
	}
	
	@Test
	public void findOrdersWhichHaveMoreThan5OrderItemsTest(){
		
		List<Order> whatShouldBe = Arrays.asList(bOrder);
		List<Order> result = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);
		assertEquals("error", whatShouldBe, result);
	}

	@Test
	public void findOldestClientAmongThoseWhoMadeOrdersTest(){

		ClientDetails whatShouldBe = cClient;
		ClientDetails result = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);
		assertEquals("error", whatShouldBe, result);
	}

	@Test
	public void findOrderWithLongestCommentsTest() {
			
		Order whatShouldBe = aOrder;
		Order result = OrdersService.findOrderWithLongestComments(orders);
		assertEquals("error", whatShouldBe, result);
	}
	
	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOldTest(){
		
		String whatShouldBe = "bName bSurname; SName cSurname";
		String result = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);
		assertEquals("error", whatShouldBe, result);
	}

	@Test
	public void getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithATest(){
	
		List<String> whatShouldBe = Arrays.asList("a","b","c","d","de");
		List<String> result = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);
		assertEquals("error", whatShouldBe, result);
	}
	
	@Test
	public void printCapitalizedClientsLoginsWhoHasNameStartingWithSTest(){

		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(orders);
		assertEquals("error", "CLOGIN", outContent.toString());
	}
	
	@Test
	public void groupOrdersByClientTest(){
	
		Map<ClientDetails, List<Order>> whatShouldBe = new HashMap<ClientDetails, List<Order>>(); 
		whatShouldBe.put(aClient, Arrays.asList(aOrder));	
		whatShouldBe.put(bClient, Arrays.asList(bOrder));
		whatShouldBe.put(cClient, Arrays.asList(cOrder));
				
		Map<ClientDetails, List<Order>> result = OrdersService.groupOrdersByClient(orders);
		assertEquals("error", whatShouldBe, result);
	}
	
	@Test
	public void partitionClientsByUnderAndOver18Test(){
		
		Map<Boolean, List<ClientDetails>> whatShouldBe = new HashMap<>(); 
		whatShouldBe.put(false, Arrays.asList(aClient));	
		whatShouldBe.put(true, Arrays.asList(bClient, cClient));
				
		Map<Boolean, List<ClientDetails>> result = OrdersService.partitionClientsByUnderAndOver18(orders);
		assertEquals("error", whatShouldBe, result);
	}
	
	@After
	public void clean(){
		System.setOut(null);
	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	